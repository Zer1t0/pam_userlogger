TITLE=pam_userlogger

LIBSRC = $(TITLE).c
LIBOBJ = $(TITLE).o
LIBSHARED = $(TITLE).so


all: $(LIBSHARED)

$(LIBSHARED): $(LIBOBJ)
	ld --shared -o $@ $(LIBOBJ) $(LDLIBS)

$(LIBOBJ): $(LIBSRC)
	$(CC) $(CFLAGS) -fPIC -c $(LIBSRC)

clean:
	rm -f *.o *.so

install: $(LIBSHARED)
	cp $(LIBSHARED)  /lib/x86_64-linux-gnu/security/
	chown root:root /lib/x86_64-linux-gnu/security/$(LIBSHARED)
	chmod 644 /lib/x86_64-linux-gnu/security/$(LIBSHARED)

#.c.o:
#	$(CC) $(CFLAGS) -Wall -c $< -o $@
