# pam_userlogger

A toy example of PAM module that logs credentials. To use you can
edit `/etc/pam.d/common-auth` and include the module.

I suggest to include it before `pam_permit` module, so you only log valid
credentials. Like this:
```
auth    [default=ignore]	pam_userlogger.so
auth	required			pam_permit.so
```

## Compilation

In order to compile it, you need to install the libpam developer library,
in Ubuntu you can install it the following way:
```
sudo apt update
sudo apt install libpam0g-dev
```

Then use make to compile:
```
make
```

If you want to compile with debug messages then:
```
make CFLAGS=-DDEBUG
```

Once the file is compiled, you will get `pam_userlogger.so` in your current
directory. You will have to copy it to the PAM modules folder. In Ubuntu it is
found in `/lib/x86_64-linux-gnu/security`, but can be another folder in your
machine.
